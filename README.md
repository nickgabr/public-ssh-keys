# Public SSH keys

My public keys, and a script to add them to `~/.ssh/authorized_keys` for use on an OpenSSH server.

Hopefully would go without saying: **don't run this script as-is on your server unless you want to give me access to it :P**  Fork the repo, use your own public key(s) instead of mine.

## Usage (server side)

1. Replace the public keys in the repo subfolder with your public key(s).
2. Clone the updated repo to server.
3. Make the `enroll_keys.sh` script executable, and run it.

## A one-liner for key enrollment (from client side)

A notable alternative approach to using this repo's `enroll_keys.sh` is the following client-side operation:

|Command                          |What it does                                |
|---------------------------------|--------------------------------------------|
|`ssh-copy-id -i ~/.ssh/id_rsa.pub <USER>@<HOST>`|**The one-liner to short-circuit this whole process:** if you presently have password-login allowed on the target SSH server (you shouldn't for very long) or another existing means to authenticate, then this should work to enroll your key remotely, while also checking and configuring the directory and file permissions.  _Here we assume the public key file is `~/.ssh/id_rsa.pub`, the default for a generated RSA type keypair._|

## Related helpful commands to know (client side prep)

The following commands represent *client-side* operations, either upstream of when one would be using this repo's script to enroll the desired public key(s) on the server, or later during everyday use of SSH or SCP.

_Here we assume the private key file is `~/.ssh/id_rsa`, the default for a generated RSA type keypair.  RSA is not the only available type, and the files could be kept elsewhere if you have reason to do so._

### Keypair generation or update

|Command                           |What it does                               |
|----------------------------------|-------------------------------------------|
|`ssh-keygen -t rsa -b 4096`       |Generate an RSA 4096-bit keypair, with default file location and names and comment (interactive: will prompt for a passphrase to encrypt the key)|
|`ssh-keygen -t rsa -b 4096 -C "<comment>"` |Generate an RSA 4096-bit keypair with specified comment string, with default file location and names (interactive: will prompt for a passphrase to encrypt the key)|
|_`ssh-keygen -y -f ~/.ssh/id_rsa`_|Returns the public key for given private key file (interactive: will prompt for a passphrase unless the key d/n have one)|
|_`ssh-keygen -e -f ~/.ssh/id_rsa`_|Exports the public key in default format (_this one can run on public key file too_)|
|_`ssh-keygen -p -f ~/.ssh/id_rsa`_|Change the passphrase for a given private key file (interactive)|
|_`ssh-keygen -c -f ~/.ssh/id_rsa`_|Change the comment associated with a given keypair (interactive)|
|_`cat ~/.ssh/id_rsa.pub`_         |Print the entire contents of the public key file|

### Basic SSH and SCP

|Command                          |What it does                                |
|---------------------------------|--------------------------------------------|
|`ssh-add`                        |Adds the default-named SSH key(s) to the session, after a one-time prompt for its passphrase|
|_`ssh-add -l`_                   |Lists some basic info about the active SSH key(s) in the session|
|_`ssh-add -D`_                   |Remove currently loaded keys from the session|
|`ssh <USER>@<HOST>`              |Open secure shell into specified server|
|`scp -p <USER>@<HOST>:<FILEPATH> <DEST>` |Secure copy the file(s) from remote host to specified local destination (_or vice versa to copy local file(s) to remote destination_)|

## References

* man page for [`sshd`](https://man7.org/linux/man-pages/man8/sshd.8.html):
  * [..."AUTHORIZED_KEYS_FILE_FORMAT" section](https://man7.org/linux/man-pages/man8/sshd.8.html#AUTHORIZED_KEYS_FILE_FORMAT)
  * [..."FILES" section](https://man7.org/linux/man-pages/man8/sshd.8.html#FILES)

## License

See the repo's `LICENSE` file.
