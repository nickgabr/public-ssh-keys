#!/bin/bash
#
# Copyright (c) 2020 Nick Gabriel.  All Rights Reserved.
#
# Meant for use on an OpenSSH server, to enroll public key(s) placed within repo
#
# Usage:
# 1. **Have the public key files you want in the 'public-keys' folder**
# 2. Make this executable: 'chmod +x enroll_keys.sh'
# 3. Run it: './enroll_keys.sh'
#

# One-stop error handling: just exit upon an error
set -o errexit

# Run our common script
source shared.sh


# =============================================================================
# Start with the SSH_DIR and its permissions
# =============================================================================

if [ -d "$SSH_DIR" ]; then
    printf "\nFound user SSH directory '%s'..." "$SSH_DIR"
else
    #TODO: consider exiting right away if there is no $SSH_DIR; maybe no openssh installed yet?
    printf "\nNo existing user SSH dir (tried '%s'), creating one..." "$SSH_DIR"
    mkdir "$SSH_DIR"
fi

# Either way, check/update its permissions
ssh_dirfile_perms "$SSH_DIR" $SSH_DIR_OCTAL


# =============================================================================
# See if there is already an authorized keys file, act accordingly
# =============================================================================

auth_keys_file_ok=-1    # initialize; we'll update from an exit code

printf "\n"

if [ -f "$AUTH_KEYS_FILE" ]; then
    printf "\nFound existing '%s'..." "$AUTH_KEYS_FILE"
else  # no AUTH_KEYS_FILE
    printf "\nNo existing auth. keys file found (tried '%s')..." "$AUTH_KEYS_FILE"
    printf "\n\tCreating empty one and setting its permissions..."
    # Create the file, with proper permissions (read-write for user only)
    touch "$AUTH_KEYS_FILE"
fi

# Check/set its permissions
auth_keys_std=$(ssh_dirfile_perms "$AUTH_KEYS_FILE" "$AUTH_KEYS_OCTAL")
auth_keys_file_ok=$?      # Collect our exit code
printf "%s" "$auth_keys_std"   #TODO: remove this print or make it conditional (verbose)

# Also consider there can be an 'authorized_keys2' file; re: format, duplicates, etc.
auth_keys_file2_ok=-1    # initialize; we'll update from an exit code

if [ -f "$AUTH_KEYS_FILE2" ]; then
    printf "\nFound existing '%s'..." "$AUTH_KEYS_FILE2"

    # Check/set its permissions
    auth_keys2_std=$(ssh_dirfile_perms "$AUTH_KEYS_FILE2" "$AUTH_KEYS_OCTAL")
    #shellcheck disable=SC2034
    auth_keys_file2_ok=$?      # Collect our exit code
    #TODO: consider doing something based on that auth_keys_file2_ok exit code
    printf "%s" "$auth_keys2_std"   #TODO: remove this print or make it conditional (verbose)

else    # no AUTH_KEYS_FILE2; fine just let user know
    printf "\n(Note: there is not a '%s')" "$AUTH_KEYS_FILE2"

fi


# ==============================================================================
# Enroll each of the keys
# ==============================================================================

#TODO: consider an option to overwrite to *only* have the repo's keys (i.e. > instead of >> during the cat)

printf "\n"

existing_keys=()    # initialize an empty array

# ...if we're ok to proceed (if our return code is zero)
printf "\nChecking for existing authorized keys..."
if [ $auth_keys_file_ok -eq 0 ]; then

    # Get the initial contents of the existing authorized keys
    # TODO: this is lazy splitting, see https://github.com/koalaman/shellcheck/wiki/SC2207
    # shellcheck disable=SC2207
    lines=( $(cat "$AUTH_KEYS_FILE") )

    # If the authorized keys aren't empty then get each of them
    if [ ${#lines} -gt 0 ]; then
        printf "\n\nExisting authorized keys:"
        # Loop through these elements, space separating them
        for token in "${lines[@]}"
            do
            # All public keys of interest start with the same prefix (after one space)
            if [[ ${token:0:4} == "$PUBKEY_PREFIX" ]]; then
                existing_keys+=( "$token" )    # push into our array
            fi
        done
        printf "\n\t%d existing authorized keys found." "${#existing_keys[@]}"
    else
        printf "\n\tNo existing keys in the file."
    fi

    printf "\n\nAttempting to enroll any new keys..."
    for pubkey_file in "$REPO_PUBKEY_DIR"/*$REPO_PUBKEY_EXT
    do
        # See if the key is already in there!  Don't want to duplicate.

        # Parse the key out of the public key file
        #TODO: should functionalize the above reading of auth keys file and reuse that here instead
        #NOTE: presently we assume one key in the file, key comes after prefix and a space
        # shellcheck disable=SC2207
        pubkey_file_contents=($(cat "$pubkey_file"))
        #printf "\n$pubkey_file_contents"
        pubkey_string=${pubkey_file_contents[*]:1:1}    # grab 1st string after a space

        #printf "\n\n$pubkey_string"
        #printf "\n\n${existing_keys[@]}\n"

        # See if the "new" key matches any existing key
        key_match=0    # initialize
        for existing_key in "${existing_keys[@]}"
        do
            # Check "new" key against this existing key
            if [[ $pubkey_string == "$existing_key" ]]; then
                key_match=1
                printf "\n\t%s's key is already in the auth. keys..." "$pubkey_file"
                #TODO: could exit the loop now too
            fi
        done
        # If not matching any, then add it
        if [ $key_match == 0 ]; then
            printf "\n\tAppending %s's contents into auth. keys..." "$pubkey_file"
            # Note: append is 'cat file1 >> file2', overwrite would be just one '>')
            # Note: Just in case, we lead and trail with a newline, to avoid
            #       "run-on" concatenation and perhaps improve human readability
            { printf "\n"; cat "$pubkey_file"; printf "\n"; } >> "$AUTH_KEYS_FILE"
        fi
     done
     printf "\nDone enrolling keys.\n"

else
    printf "\nNOT updating keys, something wrong with the %s stuff!\n" "$SSH_DIR"

fi


#TODO(maybe another script): check/revoke key(s) too?  (Cf. ssh-keygen "Key Revocation Lists")
#                            (Would also need to check for/in an authorized_keys2 file)
#                            One way to revoke is `sed -i '/<KEY>/d' ~/.ssh/authorized_keys.
