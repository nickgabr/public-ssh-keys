#!/bin/bash
#
# Copyright (c) 2020 Nick Gabriel.  All Rights Reserved.
#
# Shared constants and a function meant for use on an OpenSSH server, called by
# another script (e.g. 'source shared.sh' called from enroll_keys.sh)
#


declare -r REPO_PUBKEY_DIR="public-keys"   # name of dir in repo where keys live
declare -r REPO_PUBKEY_EXT=".pub"          # being cautious, will only grab this extension

# All 4 types of ssh-keygen public keys start " AAAA..." (after the type prefix and a space)
declare -r PUBKEY_PREFIX="AAAA"
#TODO: also use this to prevent enrolling a non-public key, e.g. a private key by accident

# Paths to SSH directory and its authorized keys file(s)
#TODO: could get these from or check them against the /etc/ssh/sshd_config's AuthorizedKeysFile

declare -r SSH_DIR="$HOME/.ssh"
# And a permission octal: we want 700 (read/write/exec for the user, not accessible by others)
declare -r SSH_DIR_OCTAL=700
# From https://man7.org/linux/man-pages/man8/sshd.8.html#FILES
#    This directory is the default location for all user-specific configuration
#    and authentication information.  There is no general requirement to keep
#    the entire contents of this directory secret, but the recommended
#    permissions are read/write/execute for the user, and not accessible by others.

declare -r AUTH_KEYS_FILE="$SSH_DIR/authorized_keys"
declare -r AUTH_KEYS_FILE2="$SSH_DIR/authorized_keys2"
# And a permission octal: we want 600 (read/write for the user, not accessible by others)
declare -r AUTH_KEYS_OCTAL=600
# From https://man7.org/linux/man-pages/man8/sshd.8.html#FILES
#    Lists the public keys (DSA, ECDSA, Ed25519, RSA) that can be
#    used for logging in as this user.  The format of this file is
#    described above.  The content of the file is not highly sensitive,
#    but the recommended permissions are read/write for the user, and
#    not accessible by others.
#
#    If this file, the ~/.ssh directory, or the user's home directory
#    are writable by other users, then the file could be modified
#    or replaced by unauthorized users.  In this case, sshd will not
#    allow it to be used unless the StrictModes option has been set
#    to "no".


# Function to check / update permissions for an SSH dir or authorized keys file
#TODO: should also check the owner to be sure it is the user
ssh_dirfile_perms() {

    # Expects 2 positional arguments as input:
    file_path=$1     # A path to the file to check/update
    stat_octal=$2    # The desired permissions octal, e.g. 600 for user-only read/write

    local file_ok=-1

    # Check/update permissions
    file_perm=$(stat -c "%a" "$file_path")

    if test "$file_perm" -eq "$stat_octal"; then
        printf "\n\tPermissions good for '%s'" "$file_path"
        file_ok=0    # normal exit code

    else
        printf "\n\tPermissions for '%s' were %s..." "$file_path" "$file_perm"
        printf "\n\tChanging its permissions to %s..." "$stat_octal"
        local chmod_std
        chmod_std=$(chmod "$stat_octal" "$file_path")  # returns nothing when good
        if [[ -z $chmod_std ]]; then
            file_ok=0    # normal exit code
        else
            # Print the output from the chmod command and give non-zero exit
            printf "\n\t%s" "$chmod_std"
            file_ok=1
        fi

    fi

    return $file_ok

}
